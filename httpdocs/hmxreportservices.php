﻿
<?php
header('Content-type: application/json; charset=utf-8');

/**
 * Arendi uygulaması web servicelerini gerçekleyen class
 * Hazırlayan - Olcay Ertaş
 */
class ArendiWebServices {

    private $db;

    // Constructor - open DB connection
    function __construct() {
        $this->db = new mysqli('localhost', 'olcayertas', '47yk2d8r6c', 'olcayertas');
        $this->db->autocommit(FALSE);
        $stmt1 = $this->db->prepare("SET NAMES 'utf8'");
        $stmt1->execute();
        $stmt2 = $this->db->prepare('SET CHARACTER SET "utf8"');
        $stmt2->execute();
    }

    // Destructor - close DB connection
    function __destruct() {
        $this->db->close();
    }

    /**
     * Proje listesini veren method.
     */
    function getProjects() {
        error_log("getProjects: giriş\n", 3, "php.log");
        $stmt = $this->db->prepare('SELECT * FROM projects');
        $id = $no = $title = $subject = $description = $attachment = $status = NULL;
        $stmt->bind_result($id, $no, $title, $subject, $description, $attachment, $status);

        if ($stmt->execute()) {
            error_log("getProjects: if\n", 3, "php.log");
            //$result = "{\"success\":1";
            $result = array();

            while ($stmt->fetch()) {
                $row = array("id" => $id, "no" => $no, "title" => $title,
                    "subject" => $subject, "description" => $description,
                    "attachment" => $attachment, "status" => $status);
                $result[] = $row;
            }

            echo json_encode($result);
        }

        $stmt->close();
        error_log("getProjects: çıkış\n", 3, "php.log");
    }

    /**
     * Giriş yapan kullanıcıyı doğrulamak için kullanılan method.
     * Kullanıcı varsa ve şifresi doğruysa kullanıcı bilgilerini döndürür.
     * @param type $email Kullanıcının email adresi
     * @param type $password Kullanıcının arendi üyelik şifresi.
     */
    function getUser($email, $password) {
        error_log("in service\n", 3, "php.log");
        $stmt = $this->db->prepare("SELECT * FROM users WHERE email=? && password=?");
        $stmt->bind_param("ss", $email, $password);
        $id = $companyid = $name = $surname = $email2 = $password2 = NULL;
        $stmt->bind_result($id, $companyid, $name, $surname, $email2, $password2);

        if ($stmt->execute()) {
            if ($stmt->fetch()) {
                $stmt->close();
                $log = $id . " - "
                        . $companyid . " - "
                        . $name . " - "
                        . $surname . " - "
                        . $email2 . " - "
                        . $password2 . "\n";
                error_log($log, 3, "php.log");
                $stmt2 = $this->db->prepare("SELECT * FROM companies WHERE id=" . $companyid);
                $cid = $cname = $caddress = $cphone = $cwebpage = NULL;
                $stmt2->bind_result($cid, $cname, $caddress, $cphone, $cwebpage);
                $stmt2->execute();
                $stmt2->fetch();
                $result = "{\"success\":1,";
                $result = $result . "\"id\":\"" . $id . "\",";
                $result = $result . "\"companyid\":\"" . $cid . "\",";
                $result = $result . "\"companyname\":\"" . $cname . "\",";
                $result = $result . "\"name\":\"" . $name . "\",";
                $result = $result . "\"surname\":\"" . $surname . "\",";
                $result = $result . "\"email\":\"" . $email2 . "\",";
                $result = $result . "\"password\":\"" . $password2 . "\"}";
                $stmt2->close();
                echo $result;
                $log = $cid . " - "
                        . $cname . " - "
                        . $caddress . " - "
                        . $cphone . " - "
                        . $cwebpage . "\n";
                error_log($log, 3, "php.log");
            } else {
                echo "{\"success\":0,\"error_message\":\"Kullanıcı adı ya da"
                . "şifre hatalı.\",\"email\":\"" . $email
                . "\",\"password\":\"" . $password . "\"}";
            }
        } else {
            echo "{\"success\":0,\"error_message\":\"Kullanıcı adı ya da "
            . "şifre hatalı.\"}";
        }
    }

    /**
     * Yeni fikir kaydetmek için kullanılan fonksiyon.
     * @param 1 - companyid - Fikir beyan eden çalışanın firmasının veritabanı idsi
     * @param 2 - employeeid - Fikir beyan edenin veritabanı idsi
     * @param 3 - title - Fikir başlığı
     * @param 4 - description - Fikri özet olarak anlatan, açıklayan metin.
     * @param 5 - privacy - Fikrin görünürlüğü: 0 ise tüm firmalarca görülebilir, 
     *            1 ise sadece fikir beyaneden kişinin firma çalışanlarınca 
     *            görülebilir.
     * @param 6 - attachment - Fikri açıklamak için kullanılan resim, video ya da ses
     *            kaydının linki
     */
    function setIdea($ideaData) {
		error_log("setIdea start\n", 3, "php.log");
		$data = json_decode($ideaData, true);
		$company = $data['company'];
		$employeeName = $data['employeeName'];
		$employeeSurname = $data['employeeSurname'];
		$title = $data['title'];
		$description = $data['description'];
		$privacy = intval($data['privacy']);
		$vote = 0;
		$attachment = $data['attachment'];
        error_log($ideaData."\n", 3, "php.log");
        $stmt = $this->db->prepare("INSERT INTO ideas VALUES ('', ?, ?, ?, ?, ?, ?, ?, ?)");
        
        if($stmt) {
        	$stmt->bind_param("sssssiis", $company, $employeeName, $employeeSurname, $title, $description, $privacy, $vote, $attachment);
        }
        else {
        	echo "{\"success\":\"0\",\"error_message\":\"Sorgu oluşturulamadı!\"}";
        	return;
        }

        if ($stmt === FALSE) {
            $code = $stmt->errorCode();
            $errInfo = $stmt->errorInfo();
            error_log("PREPARE of INSERT query, FAILED\nCode: $code ErrorMessage: $errInfo\n", 3, "php.log");
        } else {
            if ($stmt->execute()) {
                error_log("Fikir başarıyla kaydedildi!", 3, "php.log");
                echo "{\"success\":\"1\",\"success_message\":\"Fikir başarıyla kaydedildi.\"}";
            } else {
                $code = $stmt->errorCode();
                $errInfo = $stmt->errorInfo();
                error_log("INSERT FAIL!\nCode: $code ErrorMessage: $errInfo\n", 3, "php.log");
                echo "{\"success\":\"0\",\"error_message\":\"Fikir kaydedilemedi!\"}";
            }
        }
    }
}

$post = filter_input_array(INPUT_POST);
$arendiService = new ArendiWebServices();

if (isset($post['username']) && isset($post['password'])) {
    error_log("Arendi Web Services\n", 3, "php.log");
    error_log("Post data loaded.\n", 3, "php.log");
    error_log("Service instance created\n", 3, "php.log");
    error_log("service: login\n", 3, "php.log");
    error_log($post['username'] . " - " . $post['password'] . "\n", 3, "php.log");
    if ($arendiService){
        $arendiService->getUser($post['username'], $post['password']);
    }
    else {
        error_log("service: failed to create service instance\n", 3, "php.log");
    }
} else if (isset($post['setIdea'])) {
    error_log("Set Idea Services\n", 3, "php.log");
    error_log("Incoming data: ".$post['setIdea']."\n", 3, "php.log");
    if ($arendiService){
        $arendiService->setIdea($post['setIdea']);
    }
    else {
        error_log("service: failed to create service instance\n", 3, "php.log");
    }
} else if (isset($post['request'])) {
    error_log("service: getprojects\n", 3, "php.log");
    if ($post['request'] == "getprojects") {
        error_log("service: request - getprojects\n", 3, "php.log");
        $arendiService->getProjects();
    } else if ($post['request'] == "getMenu") {
        error_log("service: request - getMenu", 3, "php.log");
        $arendiService->getMenu();
    } else if ($post['request'] == "writeIdea") {
        error_log("service: request - writeIdea", 3, "php.log");
        $arendiService->getCategories();
    } else {
        error_log("service: tanımsız service\n", 3, "php.log");
        echo '{"success":0,"error_message":"Tanımsız istek!"}';
    }
} else {
    error_log("Geçersiz service talebi!\n", 3, "php.log");
    echo '{"success":0,"error_message":"Geçersiz service talebi!"}';
}

