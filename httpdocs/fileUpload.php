
<?php

try { 
    // Undefined | Multiple Files | $_FILES Corruption Attack
    // If this request falls under any of them, treat it invalid.
    if (!isset($_FILES['upfile']['error']) ) {
    	//|| is_array($_FILES['upfile']['error'])
        throw new RuntimeException('Invalid parameters.');
        error_log("File Upload: Invalid parameters.\n", 3, "upload.log");
    }
    
    // Check $_FILES['upfile']['error'] value.
    switch ($_FILES['upfile']['error']) {
        case UPLOAD_ERR_OK:
        	error_log("File Upload: No error.\n", 3, "upload.log");
            break;
        case UPLOAD_ERR_NO_FILE:
            throw new RuntimeException('No file sent.');
            error_log("File Upload: No file sent.\n", 3, "upload.log");
        case UPLOAD_ERR_INI_SIZE:
        case UPLOAD_ERR_FORM_SIZE:
            throw new RuntimeException('Exceeded filesize limit.');
            error_log("File Upload: Exceeded filesize limit.\n", 3, "upload.log");
        default:
            throw new RuntimeException('Unknown errors.');
            error_log("File Upload: Unknown errors.\n", 3, "upload.log");
    }
    
    // You should also check filesize here.
    if ($_FILES['upfile']['size'] > 1000000) {
        throw new RuntimeException('Exceeded filesize limit.');
        error_log("File Upload: Exceeded filesize limit.\n", 3, "upload.log");
    }
	
	error_log("File Upload: Filesize limit OK.\n", 3, "upload.log");
	
	$uploaddir = 'arendi/';
	$username = basename($_FILES['upfile']['username']);
	$filename = basename($_FILES['upfile']['filename']);
	$uploadfile = $uploaddir . $username . '/' . $filename;
	
	error_log("File Upload: upload file= " . $uploadfile . "\n", 3, "upload.log");

	if (move_uploaded_file($_FILES['upfile']['tmp_name'], $uploadfile)) {
    	echo $file;
    	error_log("File Upload: File uploaded.\n", 3, "upload.log");
	} else {
    	echo "File upload failed.";
    	error_log("File Upload: File upload failed.\n", 3, "upload.log");
	}
	
} catch (RuntimeException $e) {
    echo $e->getMessage();
    error_log("File Upload: Exception " . $e->getMessage(), 3, "upload.log");
}

?>