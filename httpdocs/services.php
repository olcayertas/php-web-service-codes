<?php

/**
 * Arendi web servis kodlarını içeren dosyadır.
 * @category Arendi
 * @package Arendi
 * @author Olcay Ertaş <olcayertas@gmail.com>
 * @license http://www.olcayertas.com/Arendi/ArendiLicense.txt Lisans
 * @version GIT:
 * @link http://www.arendi.com Arendi: Kurumsal sosyal ağ
 */
header('Content-type: application/json; charset=utf-8');

use Psr\Log\LogLevel;

require 'vendor/autoload.php';

/**
 * Arendi uygulaması web servicelerini gerçekleyen sınıftır.
 * @author Olcay Ertaş <olcayertas@gmail.com>
 */
class ArendiWebServices
{
    private $mysqlConnection;
    private $host = "localhost";
    private $userName = "olcayertas";
    private $password = "47yk2d8r6c";
    private $database = "olcayertas";
    private $logger;
    private $statement;

    /**
     * Constroctor.
     * Initializes database connection with hard coded user name, password, host 
     * address and database name. Also initializes KLogger.
     */
    function __construct()
    {
        $this->mysqlConnection
            = new mysqli(
                $this->host, $this->userName, $this->password, $this->database
            );
        $this->mysqlConnection->autocommit(false);
        $stmt1 = $this->mysqlConnection->prepare("SET NAMES 'utf8'");
        $stmt1->execute();
        $stmt2 = $this->mysqlConnection->prepare('SET CHARACTER SET "utf8"');
        $stmt2->execute();
        $this->logger = new Katzgrau\KLogger\Logger("logs", LogLevel::DEBUG);
    }

    /**
     * Destructor.
     * Closes database connection.
     */
    function __destruct()
    {
        $this->mysqlConnection->close();
    }

    /**
     * Returns project list as JSON.
     * @return JSON Returns the project list.
     */
    function getProjects()
    {
        $this->logger->debug("GetProjects: Entrance\n");
        $stmt = $this->mysqlConnection->prepare('SELECT * FROM arendi_projects');

        $id = null;
        $no = null;
        $title = null;
        $subject = null;
        $description = null;
        $attachment = null;
        $status = null;

        $stmt->bind_result(
            $id, $no, $title, $subject, $description, $attachment, $status
        );

        if ($stmt->execute()) {
            $this->logger->debug("GetProjects: If\n");
            $result = array();

            while ($stmt->fetch()) {
                $row = array("id" => $id, "no" => $no, "title" => $title,
                    "subject" => $subject, "description" => $description,
                    "attachment" => $attachment, "status" => $status);
                $result[] = $row;
            }

            echo json_encode($result);
        }

        $stmt->close();
        $this->logger->debug("GetProjects: Exit\n");
    }

    /**
     * Returns user information as JSON.
     * @param type $email    Email address of the requested user.
     * @param type $password Account password of the user.
     * @return string Returns user information as JSON or returns
     * error message as JSON.
     */
    function getUser($email, $password)
    {
        $this->logger->debug("");
        $this->logger->debug("GetUser: Getting user info...\n");
        $stmt = $this->mysqlConnection->prepare(
            "SELECT * FROM arendi_users WHERE userEmail=? && userPassword=?"
        );
        $stmt->bind_param("ss", $email, $password);
        $this->getUserWithStatement($stmt);
    }

    /**
     * Returns the user with given id.
     * @param type $userId Id of the user requested.
     * @return type Description
     */
    function getUserWithId($userId)
    {
        $this->logger->debug("");
        $this->logger->debug("GetUser: Getting user info...\n");
        $stmt = $this->mysqlConnection->prepare("SELECT * FROM arendi_users WHERE id=?");
        $stmt->bind_param("i", $userId);
        $this->getUserWithStatement($stmt);
    }

    /**
     * Returns the user that given sql statement provides.
     * @param type $statement
     */
    function getUserWithStatement($statement)
    {
        $userId = $userCompanyId = $userName = $userSurname = $userEmail = $userPhone = $password = $role = null;
        $statement->bind_result($userId, $userCompanyId, $userName, $userSurname, $userEmail, $userPhone, $userPassword, $role);

        if ($statement->execute()) {
            $responseArray = array();
            
            while ($statement->fetch()) {
                $user = array();
                $user["userId"]       = "$userId";
                $user["companyId"]    = "$userCompanyId";
                $user["userName"]     = "$userName";
                $user["userSurname"]  = "$userSurname";
                $user["userEmail"]    = "$userEmail";
                $user["userPhone"]    = "$userPhone";
                $user["userPassword"] = "$userPassword";
                $user["userRole"]     = "$role";
                $responseArray[] = $user;
                $this->logger->debug(json_encode($user));
            }
            
            foreach ($responseArray as $aUser) {
                $companyId = $companyName = $companyAddress = $companyPhone = $companyWebPage = null;
                $queryString = 'SELECT * FROM arendi_companies WHERE id=' . $aUser["companyId"];
                $this->logger->debug($queryString);
                $statement2 = $this->mysqlConnection->prepare($queryString);
                $statement2->bind_result($companyId, $companyName, $companyAddress, $companyPhone, $companyWebPage);
                $statement2->execute();
                $statement2->fetch();
                $statement2->close();
                $aUser["companyName"]    = "$companyName";
                $aUser["companyAddress"] = "$companyAddress";
                $aUser["companyPhone"]   = "$companyPhone";
                $aUser["companyWebPage"] = "$companyWebPage";
            }
            
            $statement->close();
            echo json_encode($responseArray);
        } else {
            $this->jsonResponseWithErrorMessage("Sorgu çalıştırılamadı");
        }
    }

    /**
     * Yeni fikir kaydetmek için kullanılan fonksiyon.
     * @param 1 - companyid - Fikir beyan eden çalışanın firmasının veritabanı idsi
     * @param 2 - employeeid - Fikir beyan edenin veritabanı idsi
     * @param 3 - title - Fikir başlığı
     * @param 4 - description - Fikri özet olarak anlatan, açıklayan metin.
     * @param 5 - privacy - Fikrin görünürlüğü: 0 ise tüm firmalarca görülebilir, 
     *            1 ise sadece fikir beyaneden kişinin firma çalışanlarınca 
     *            görülebilir.
     * @param 6 - attachment - Fikri açıklamak için kullanılan resim, video ya da ses
     *            kaydının linki
     */
    function saveIdea($ideaData)
    {
        $this->logger->debug("SaveIdea: Start");
        $data = $ideaData;
        $companyId = $data['companyId'];
        $companyName = $data['companyName'];
        $userId = $data['userId'];
        $userName = $data['userName'];
        $userSurname = $data['userSurname'];
        $ideaTitle = $data['ideaTitle'];
        $ideaDetail = $data['ideaDetail'];
        $commentsCount = 0;
        $likesCount = 0;
        $attachment = null;

        if (array_key_exists('attachment', $data)) {
            $attachment = $data['attachment'];
        }

        $creationDate = $date = date('d.m.Y H:i:s');
        $this->logger->debug("SaveIdea: Current date: " . $creationDate);
        $this->logger->debug("SaveIdea: Preparing query...");
        $stmt = $this->mysqlConnection->prepare("INSERT INTO arendi_ideas VALUES ('', ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");

        if ($stmt) {
            $this->logger->debug("SaveIdea: Binding prameters...");
            $stmt->bind_param(
                "isissssiiss", $companyId, $companyName, $userId, 
                $userName, $userSurname, $ideaTitle, $ideaDetail, 
                $commentsCount, $likesCount, $attachment, $creationDate
            );
        } else {
            $this->logger->debug("SaveIdea: Preparing query failed!");
            $this->jsonResponseWithErrorMessage("Sorgu oluşturulamadı!");
        }

        if ($stmt === false) {
            $code = $stmt->errorCode();
            $errInfo = $stmt->errorInfo();
            $this->logger->debug("Prepare of INSERT query, failed!\nCode: $code ErrorMessage: $errInfo\n");
            $this->jsonResponseWithErrorMessage("Fikir kaydedilemedi!");
        } else {

            if ($stmt->execute()) {
                $this->logger->debug("SaveIdea: Idea saved!");
                $responseArray = array();
                $responseArray["success"] = "1";
                $responseArray["successMessage"] = "Fikir başarıyla kaydedildi.";
                $this->logger->debug("SaveIdea: Response:\n" . json_encode($responseArray));
                echo json_encode($responseArray);
            } else {
                $code = $stmt->errorCode();
                $errInfo = $stmt->errorInfo();
                $this->logger->error("SaveIdea: Failed to save idea!");
                $this->logger->error("SaveIdea: Code: $code ErrorMessage: $errInfo\n");
                $this->jsonResponseWithErrorMessage("Fikir kaydedilemedi!");
            }

            $this->logger->debug("SaveIdea: Stop");
        }
    }
    
    /**
     * Updates the given idea.
     * @param type $ideaData Updated idea data.
     */
    function updateIdea($ideaData)
    {
        $this->logger->debug("UpdateIdea: Start");
        $ideaId = $ideaData['ideaId'];
        $ideaTitle = $ideaData['ideaTitle'];
        $ideaDetail = $ideaData['ideaDetail'];
        $queryString = 'UPDATE arendi_ideas SET ideaTitle = "'.$ideaTitle.'", ideaDetail = "'.$ideaDetail.'" WHERE id = '.$ideaId;
        $this->logger->debug("UpdateIdea: $queryString");
        $stmt = $this->mysqlConnection->prepare($queryString);
        
        if ($stmt === false) {
            $code = $stmt->errorCode();
            $errInfo = $stmt->errorInfo();
            $this->logger->debug("Prepare of update query, failed! Code: $code ErrorMessage: $errInfo\n");
            $this->jsonResponseWithErrorMessage("Fikir güncellenemedi!");
        } else {

            if ($stmt->execute()) {
                $this->logger->debug("UpdateIdea: Idea updated!");
                $responseArray = array();
                $responseArray["success"] = "1";
                $responseArray["successMessage"] = "Fikir başarıyla güncellendi.";
                $this->logger->debug("SaveIdea: Response:\n" . json_encode($responseArray));
                echo json_encode($responseArray);
            } else {
                $code = $stmt->errorCode();
                $errInfo = $stmt->errorInfo();
                $this->logger->error("UpdateIdea: Failed to update idea!");
                $this->logger->error("UpdateIdea: Code: $code ErrorMessage: $errInfo\n");
                $this->jsonResponseWithErrorMessage("Fikir kaydedilemedi!");
            }

            $this->logger->debug("UpdateIdea: Stop");
        }
    }

    /**
     * Fetches and returns ideas as specified by $numberOfIdeasToFetch starting
     * from $staringIdeaIndex in chronological order;
     * @param type $startingIdeaIndex
     * @param type $numberOfIdeasToFetch
     * @return type Returns the number of ideas requested
     * staring from given index.
     */
    function getIdeas($startingIdeaIndex, $numberOfIdeasToFetch)
    {
        $this->logger->debug("");
        $this->logger->debug("GetIdeas: Getting $numberOfIdeasToFetch ideas "
            . "from index $startingIdeaIndex ..."
        );

        if (!$startingIdeaIndex) {
            $startingIdeaIndex = 0;
        }

        $statement = $this->mysqlConnection->prepare(
            "SELECT * FROM arendi_ideas ORDER BY creationDate DESC LIMIT "
            . "$startingIdeaIndex, $numberOfIdeasToFetch"
        );
        $this->getIdeasForStatement($statement);
    }

    /**
     * Returns ideas that newest than given date.
     * @param type $startingDate Staring date for newest ideas.
     * @returns An array of newest ideas.
     * @return type Returns the ideas newer than given date.
     */
    function getIdeasNewerThan($startingDate)
    {
        $this->logger->debug("");
        $this->logger->debug(
            "GetIdeasNewerThan: Getting ideas newer than $startingDate"
        );

        if (!$startingDate) {
            $this->jsonResponseWithErrorMessage("Invalid starting date!");
        }

        $queryString = 'SELECT * FROM arendi_ideas WHERE (creationDate > "' 
            . $startingDate . '") = 1 ORDER BY creationDate DESC';
        $this->logger->debug("GetIdeasNewerThan: Query string = $queryString");
        $statement = $this->mysqlConnection->prepare($queryString);

        if (!$statement) {
            $this->jsonResponseWithErrorMessage("Sorgu oluşturulamadı!");
        }

        $this->getIdeasForStatement($statement);
    }

    /**
     * Returns ideas of user with $userId up to number specified by 
     * $numberOfIdeasToFetch parameter.
     * @param type $userId User id of the user whose ideas requested.
     * @param int $startingIdeaIndex Starting index of the requested ideas.
     * @param type $numberOfIdeasToFetch Number of the ideas to get.
     */
    function getIdeasForUser($userId, $startingIdeaIndex, $numberOfIdeasToFetch)
    {
        $this->logger->debug("");
        $this->logger->debug(
            "GetIdeasForUser: Getting $numberOfIdeasToFetch ideas from index "
                . "$startingIdeaIndex for user with id $userId");

        if (!$startingIdeaIndex) {
            $startingIdeaIndex = 0;
        }

        $statementString = 
            "SELECT * FROM arendi_ideas WHERE userId = $userId ORDER BY "
             . "creationDate DESC LIMIT $startingIdeaIndex, $numberOfIdeasToFetch";
        $this->logger->debug("$statementString");
        $statement = $this->mysqlConnection->prepare($statementString);
        $this->getIdeasForStatement($statement);
    }

    /**
     * Bir kullanıcının beğendi fikirleri verir.
     * @param type $userId Kullanıcı numarası
     */
    function getIdeasThatUserLiked($userId, $startingIdeaIndex, $numberOfIdeasToFetch)
    {
        $this->logger->debug("");
        $this->logger->debug("GetIdeasThatILiked: Getting ideas that user:$userId liked ");
        $statementString = "SELECT * FROM arendi_likes WHERE (userId = $userId and likedItemType = 0) LIMIT $startingIdeaIndex, $numberOfIdeasToFetch";
        $this->logger->debug("$statementString");
        $statement = $this->mysqlConnection->prepare($statementString);
        $likeId = $likedItemId = $likedItemType = $userId = null;
        $statement->bind_result($likeId, $likedItemId, $likedItemType, $userId);
        
        /** Kullanıcının beğendiği fikirlerin idlerini bul */
        if ($statement->execute()) {
            $likes = array();

            while ($statement->fetch()) {
                $row = array();
                $row["likeId"] = $likeId;
                $row["likedItemId"] = $likedItemId;
                $row["likedItemType"] = $likedItemType;
                $row["userId"] = $userId;
                $likes[] = $row;
            }

            $statement->close();
            $responseArray = array();
            
            /** Kullanıcının beğendiği her fikri getir */
            foreach ($likes as $like)
            {
                $ideaId = $like['likedItemId'];
                $statementString = "SELECT * FROM arendi_ideas WHERE id = $ideaId";
                $statement = $this->mysqlConnection->prepare($statementString);
                $ideaId = $companyId = $companyName = $userId = $userName = null;
                $userSurname = $ideaTitle = $ideaDetail = $ideaCommentsCount = null;
                $ideaLikesCount = $ideaAttachments = $ideaCreationDate = null;
                $statement->bind_result(
                    $ideaId, $companyId, $companyName, $userId, $userName, 
                    $userSurname, $ideaTitle, $ideaDetail, $ideaCommentsCount, 
                    $ideaLikesCount, $ideaAttachments, $ideaCreationDate
                );
                
                if ($statement->execute()) {
                    $statement->fetch();
                    $idea = array();
                    $idea["ideaId"] = "$ideaId";
                    $idea["companyId"] = "$companyId";
                    $idea["companyName"] = "$companyName";
                    $idea["userId"] = "$userId";
                    $idea["userName"] = "$userName";
                    $idea["userSurname"] = "$userSurname";
                    $idea["ideaTitle"] = "$ideaTitle";
                    $idea["ideaDetail"] = "$ideaDetail";
                    $idea["ideaCommentsCount"] = "$ideaCommentsCount";
                    $idea["ideaLikesCount"] = "$ideaLikesCount";
                    $idea["ideaAttachments"] = "$ideaAttachments";
                    $idea["ideaCreationDate"] = "$ideaCreationDate";
                    $responseArray[] = $idea;
                }
                
                 $statement->close();
            }
            
            echo json_encode($responseArray);
        } else {
            $this->jsonResponseWithErrorMessage("Query failed!");
        }
    }

    /**
     * Returns most liked $numberOfIdeasToFetch ideas
     * @param int $startingIdeaIndex
     * @param type $numberOfIdeasToFetch
     */
    function getMostLikedIdeas($startingIdeaIndex, $numberOfIdeasToFetch)
    {
        $this->logger->debug("");
        $this->logger->debug("GetMostLikedIdeas: Getting $numberOfIdeasToFetch ideas from index $startingIdeaIndex");

        if (!$startingIdeaIndex) {
            $startingIdeaIndex = 0;
        }

        $statementString = "SELECT * FROM arendi_ideas ORDER BY likesCount DESC LIMIT $startingIdeaIndex, $numberOfIdeasToFetch";
        $this->logger->debug("$statementString");
        $statement = $this->mysqlConnection->prepare($statementString);
        $this->getIdeasForStatement($statement);
    }

    /**
     * Gets ideas for prepared statement.
     * @return type Returns the ideas that given sql statement effects.
     */
    function getIdeasForStatement($statement)
    {
        $this->logger->debug("GetIdeasForStatement: Begin");
        $ideaId = $companyId = $companyName = $userId = $userName = null;
        $userSurname = $ideaTitle = $ideaDetail = $ideaCommentsCount = null;
        $ideaLikesCount = $ideaAttachments = $ideaCreationDate = null;
        $this->logger->debug("GetIdeasForStatement: Binding results");
        $statement->bind_result(
            $ideaId, $companyId, $companyName, $userId, $userName, 
            $userSurname, $ideaTitle, $ideaDetail, $ideaCommentsCount, 
            $ideaLikesCount, $ideaAttachments, $ideaCreationDate
        );
        $responseArray = array();

        if ($statement->execute()) {
            while ($statement->fetch()) {
                $row = array();
                $row["ideaId"] = "$ideaId";
                $row["companyId"] = "$companyId";
                $row["companyName"] = "$companyName";
                $row["userId"] = "$userId";
                $row["userName"] = "$userName";
                $row["userSurname"] = "$userSurname";
                $row["ideaTitle"] = "$ideaTitle";
                $row["ideaDetail"] = "$ideaDetail";
                $row["ideaCommentsCount"] = "$ideaCommentsCount";
                $row["ideaLikesCount"] = "$ideaLikesCount";
                $row["ideaAttachments"] = "$ideaAttachments";
                $row["ideaCreationDate"] = "$ideaCreationDate";
                $responseArray[] = $row;
            }

            $this->logger->debug("GetIdeasForStatement: End good");
            echo json_encode($responseArray);
        } else {
            $this->logger->debug("GetIdeasForStatement: End bad");
            $this->jsonResponseWithErrorMessage("Query failed!");
        }
    }

    /**
     * Returns the number of ideas of user has created which specified with $userId.
     * @param type $userId Id number of the user whose idea count requested.
     */
    function getIdeasCountForUser($userId)
    {
        $this->logger->debug("");
        $this->logger->debug("GetIdeasCountForUser");
        $statementString = "SELECT * FROM arendi_ideas WHERE userId = " . $userId;
        $statement = $this->mysqlConnection->prepare($statementString);

        $ideaId = $companyId = $companyName = $userId = $userName = null;
        $userSurname = $ideaTitle = $ideaDetail = $ideaCommentsCount = null;
        $ideaLikesCount = $ideaAttachments = $ideaCreationDate = null;

        $statement->bind_result(
            $ideaId, $companyId, $companyName, $userId, $userName, 
            $userSurname, $ideaTitle, $ideaDetail, $ideaCommentsCount, 
            $ideaLikesCount, $ideaAttachments, $ideaCreationDate
        );
        $responseArray = array();

        if ($statement->execute()) {
            while ($statement->fetch()) {
                $row = array();
                $row["ideaId"] = "$ideaId";
                $row["companyId"] = "$companyId";
                $row["companyName"] = "$companyName";
                $row["userId"] = "$userId";
                $row["userName"] = "$userName";
                $row["userSurname"] = "$userSurname";
                $row["ideaTitle"] = "$ideaTitle";
                $row["ideaDetail"] = "$ideaDetail";
                $row["ideaCommentsCount"] = "$ideaCommentsCount";
                $row["ideaLikesCount"] = "$ideaLikesCount";
                $row["ideaAttachments"] = "$ideaAttachments";
                $row["ideaCreationDate"] = "$ideaCreationDate";
                $responseArray[] = $row;
            }

            $response = array();
            $response["success"] = "1";
            $response["count"] = count($responseArray);
            echo json_encode($response);
        } else {
            $this->logger->debug("GetIdeasForStatement: End bad");
            $this->jsonResponseWithErrorMessage("Query failed!");
        }
    }

    /**
     * Bir fikir ya da yorum için yeni bir beğeni kaydı oluşturur.
     * @param type $likeData Oluşturulacak beğeni için bilgiler içerir:
     * Beğenen kişi numarası, beğenilen fikir numarası, beğinilen şeyin tipi, 
     * fikir ya da yorum olabilir.
     * @return type Returns 1 if successful else returns 0.
     */
    function setLikeForItem($likeData)
    {
        $this->logger->debug("");
        $this->logger->debug("SetLikeForItem");
        $queryString = 'INSERT INTO arendi_likes (likeId, likedItemId, likedItemType, userId) VALUES ("", "' 
                . $likeData['likedItemId'] . '", "' 
                . $likeData['likedItemType'] . '", "' 
                . $likeData['userId'] . '")';
        $this->logger->debug("SetLikeForItem: Query string = $queryString");
        $statement = $this->mysqlConnection->prepare($queryString);

        if ($statement->execute()) {
            $statement->fetch();
            $statement->close();
            $this->logger->debug("SetLikeForItem: Query Executed");
            $ideaId = $likeData['likedItemId'];
            $queryString = "UPDATE arendi_ideas SET likesCount = likesCount + 1 WHERE id = $ideaId";
            $this->logger->debug("SetLikeForItem: Query string = $queryString");
            $statement = $this->mysqlConnection->prepare($queryString);
            $statement->execute();
            $statement->close();
            $responseArray = array();
            $responseArray["success"] = "1";
            echo json_encode($responseArray);
        }
        else {
            $this->jsonResponseWithErrorMessage("Query failed!");
        }
    }

    /**
     * Bir fikir ya da yorum için var olan beğeni kaydını siler.
     * @param type $likeData Silinecek beğeni bilgisi:
     * Beğenen kişi numarası, beğenilen fikir numarası, beğinilen şeyin tipi, 
     * fikir ya da yorum olabilir.
     */
    function removeLikeForItem($likeData)
    {
        $this->logger->debug("");
        $this->logger->debug("RemoveLikeForItem");
        $queryString =
                'DELETE FROM arendi_likes WHERE ('
                . 'likedItemType = ' . $likeData['likedItemType'] . ' and '
                . 'likedItemId = '   . $likeData['likedItemId'] . ' and '
                . 'userId = '        . $likeData['userId'] . ')';
        $this->logger->debug("RemoveLikeForItem: Query string = $queryString");
        $statement = $this->mysqlConnection->prepare($queryString);

        if ($statement->execute()) {
            $this->logger->debug("RemoveLikeForItem: Query Executed");
            $statement->fetch();
            $statement->close();
            $this->logger->debug("SetLikeForItem: Query Executed");
            $ideaId = $likeData['likedItemId'];
            $queryString = "UPDATE arendi_ideas SET likesCount = likesCount - 1 WHERE id = $ideaId";
            $this->logger->debug("SetLikeForItem: Query string = $queryString");
            $statement = $this->mysqlConnection->prepare($queryString);
            $statement->execute();
            $statement->close();
            $responseArray = array();
            $responseArray["success"] = "1";
            echo json_encode($responseArray);
        } else {
            $this->jsonResponseWithErrorMessage("Query failed!");
        }
    }

    /**
     * Bir fikir ya da yorumun beğeni sayısını verir.
     * @param type $itemData Beğeni sayısı istenen nesne bilgisi: Nesnenin 
     * tipini, yorum ya da fikir, ve nesne numarasını barındırır.
     */
    function getLikesCountForItem($itemData)
    {
        $this->logger->debug("");
        $this->logger->debug("GetLikesCountForItem");
        $queryString = 'SELECT * FROM arendi_likes WHERE (likedItemType = ' 
            . $itemData['likedItemType'] . ' and likedItemId = ' 
            . $itemData['likedItemId'] . ')';
        $this->logger->debug("GetLikesCountForItem: Query string = $queryString");
        $statement = $this->mysqlConnection->prepare($queryString);
        $this->getLikesCountForStatement($statement);
        $this->logger->debug("GetLikesCountForItem: Finish.");
    }

    /**
     * Returns if item is liked by user.
     * @param type $likeData Contains item and user data.
     * @return type Returns 0 if user didn't like item else returns 1.
     */
    function isItemLikedByUser($likeData)
    {
        $this->logger->debug("IsItemLikedByUser");
        $queryString = 
                  'SELECT * FROM arendi_likes WHERE ('
                . 'likedItemType = '    . $likeData['likedItemType'] 
                . ' and userId = '      . $likeData['userId'] 
                . ' and likedItemId = ' . $likeData['likedItemId'] . ')';
        $this->logger->debug("IsItemLikedByUser: Query string = $queryString");
        $statement = $this->mysqlConnection->prepare($queryString);
        $this->getLikesCountForStatement($statement);
        $this->logger->debug("IsItemLikedByUser: Finish.");
    }

    /**
     * Returns likes count for sql statement.
     * This is a common method for getting likes count for a prepared query.
     * @param type $statement
     * @return type Returns number of likes for item.
     */
    function getLikesCountForStatement($statement)
    {
        $this->logger->debug("GetLikesCountForStatement");
        $likeId = $likedItemId = $likedItemType = $userId = null;
        $statement->bind_result($likeId, $likedItemId, $likedItemType, $userId);
        
        if ($statement) {
            if ($statement->execute()) {
                $likes = array();
                
                while ($statement->fetch()) {
                    $row = array();
                    $row["likeId"] = $likeId;
                    $row["likedItemId"] = $likedItemId;
                    $row["likedItemType"] = $likedItemType;
                    $row["userId"] = $userId;
                    $likes[] = $row;
                }
                
                $responseArray = array();
                $responseArray["success"] = "1";
                $responseArray["likesCount"] = count($likes);
                echo json_encode($responseArray);
            } else {
                $this->jsonResponseWithErrorMessage("Query failed!");
            }
        } else {
            $this->jsonResponseWithErrorMessage("Query null!");
        }
    }

    /**
     * Returns an erros message in json format ith given error message.
     * @param type $errorMessasge Error message.
     * @return type Returns a json string that contains success status and a
     * message that describes the error.
     */
    function jsonResponseWithErrorMessage($errorMessasge)
    {
        $responseArray = array();
        $responseArray["success"] = "0";
        $responseArray["errorMessage"] = $errorMessasge;
        echo json_encode($responseArray);
    }

    /**
     * Bir fikir için yeni bir yorum kaydı oluşturur.
     * @param type $commentData Yorum bilgilerini içerir: Yorum metni, yorum 
     * yapan kullanıcının numarası, yorum yapılan fikir numarası.
     */
    function setCommentForIdea($commentData)
    {
        $this->logger->debug("");
        $this->logger->debug("SetCommentForIdea");
        $likeCount = 0;
        $date = date('d.m.Y H:i:s');
        $queryString =
            'INSERT INTO arendi_comments (id, comment, date, likeCount, userId, ideaId) VALUES ("", "' 
            . $commentData['comment'] . '", "' . $date . '", ' 
            . $likeCount . ', ' . $commentData['userId'] . ', ' 
            . $commentData['ideaId'] . ')';
        $this->logger->debug("SetCommentForIdea: Query string = $queryString");
        $statement = $this->mysqlConnection->prepare($queryString);
        $this->setCommentForStatement($statement);
    }
    
    /**
     * Creates a new comment record for given statement.
     * @param type $statement Statement to be executed.
     */
    function setCommentForStatement($statement)
    {
        if ($statement->execute()) {
            $this->logger->debug("SetCommentForStatement: Query Executed");
            $responseArray = array();
            $responseArray["success"] = "1";
            echo json_encode($responseArray);
        }
        else {
            $this->jsonResponseWithErrorMessage("SetCommentForStatement: Query failed!");
        }
    }

    /**
     * Bir fikrin yorum sayısını verir.
     * @param type $ideaId Yorum sayısı istenen fikrin numarası.
     */
    function getCommentsCountForIdea($ideaId)
    {
        $this->logger->debug("");
        $this->logger->debug("GetCommentsCountForIdea");
        $queryString = "SELECT * FROM arendi_comments WHERE (ideaId = $ideaId)";
        $this->logger->debug("GetCommentsCountForIdea: Query string = $queryString");
        $statement = $this->mysqlConnection->prepare($queryString);

        $id = $comment = $date = $likeCount = $userId = $ideaId = null;
        $statement->bind_result($id, $comment, $date, $likeCount, $userId, $ideaId);
        
        if ($statement) {
            if ($statement->execute()) {
                $comments = array();
                
                while ($statement->fetch()) {
                    $row = array();
                    $comments[] = $row;
                }
                
                $responseArray = array();
                $responseArray["success"] = "1";
                $responseArray["commentsCount"] = count($comments);
                echo json_encode($responseArray);
            } else {
                $this->jsonResponseWithErrorMessage("GetCommentsCountForIdea: Query failed!");
            }
        } else {
            $this->jsonResponseWithErrorMessage("GetCommentsCountForIdea: Query null!");
        }
    }

    /**
     * Bir fikrin yorumlarını verir.
     * @param type $ideaId Yorumları istenen fikrin numarası.
     */
    function getCommentsForIdea($ideaId)
    {
        $this->logger->debug("");
        $this->logger->debug("GetCommentsForIdea");
        $statement = $this->mysqlConnection->prepare("SELECT * FROM arendi_comments WHERE ideaId = $ideaId");
        $id = $comment = $date = $likeCount = $userId = $ideaId = null;
        $statement->bind_result($id, $comment, $date, $likeCount, $userId, $ideaId);
    
        if ($statement->execute()) {
            $responseArray = array();
            
            while ($statement->fetch()) {
                $row = array();
                $row["id"] = $id;
                $row["comment"] = $comment;
                $row["date"] = $date;
                $row["likeCount"] = $likeCount;
                $row["userId"] = $userId;
                $row["ideaId"] = $ideaId;
                $responseArray[] = $row;
            }

            $this->logger->debug("GetCommentsForIdea: End good");
            echo json_encode($responseArray);
        } else {
            $this->logger->debug("GetCommentsForIdea: End bad");
            $this->jsonResponseWithErrorMessage("GetCommentsForIdea: Query failed!");
        }
    }
    
    /**
     * Returns the users that its name or surname contains given search string.
     * @param type $searchString Search string.
     * @return type Returns the users which it's name or surname contains 
     * search string.
     */
    function getUsersForSearchString($searchString)
    {
        $this->logger->debug("GetUserForSearchString");
        $queryString = "SELECT * FROM arendi_users WHERE (userName LIKE \"%$searchString%\" or userSurname LIKE \"%$searchString%\" or userEmail LIKE \"%$searchString%\")";
        $this->logger->debug("GetUserForSearchString: Query string = $queryString");
        $statement = $this->mysqlConnection->prepare($queryString);
        $this->getUserWithStatement($statement);
    }
    
    /**
     * Arama metnine uyan fikirleri getirir.
     * @param type $searchString
     */
    function getIdeasForSearchString($searchString)
    {
        $this->logger->debug("GetIdeasForSearchString");
        $queryString = "SELECT * FROM arendi_ideas WHERE (ideaTitle LIKE \"%$searchString%\" or ideaDetail LIKE \"%$searchString%\" or userName LIKE \"%$searchString%\" or userSurname LIKE \"%$searchString%\")";
        $this->logger->debug("GetIdeasForSearchString: Query string = $queryString");
        $statement = $this->mysqlConnection->prepare($queryString);
        $this->getIdeasForStatement($statement);
    }
}

$arendiService = new ArendiWebServices();
$content = file_get_contents('php://input');
$post = json_decode($content, true);
$logger = new Katzgrau\KLogger\Logger("logs", LogLevel::DEBUG);
$logger->debug("Arendi Web Services");
$logger->debug("Post data loaded");
$logger->debug("Service instance created");

if (isset($post['request'])) {
    /** Login service */
    if ($post['request'] === 'login') {
        $logger->debug("Requested service - Login");
        $logger->debug($post['userName'] . " - " . $post['password']);
        $arendiService->getUser($post['userName'], $post['password']);
    }
    /** Get user with id */
    else if ($post['request'] === 'getUserWithId') {
        $logger->debug("Requested service - GetUserWithId");
        $arendiService->getUserWithId($post['userId']);
    }
    /** Get users for search string */
    else if ($post['request'] === 'getUsersForSearchString') {
        $logger->debug("Requested service - GetUsersForSearchString");
        $arendiService->getUsersForSearchString($post['searchString']);
    }
    /** Get ideas */
    else if ($post['request'] === 'getIdeas') {
        $logger->debug("Requested service - GetIdeas");
        $arendiService->getIdeas($post['startingIdeaIndex'], $post['numberOfIdeasToFetch']);
    }
    /** Get ideas for user */
    else if ($post['request'] === 'getIdeasForUser') {
        $logger->debug("Requested service - GetIdeasForUser");
        $arendiService->getIdeasForUser($post['userId'], $post['startingIdeaIndex'], $post['numberOfIdeasToFetch']);
    }
    /** Get ideas newer that given date */
    else if ($post['request'] === 'getIdeasNewerThan') {
        $logger->debug("Requested service - GetIdeasNewerThan");
        $arendiService->getIdeasNewerThan($post['startingDate']);
    }
    /** Get ideas for search string */
    else if ($post['request'] === 'getIdeasForSearchString') {
        $logger->debug("Requested service - GetIdeasForSearchString");
        $arendiService->getIdeasForSearchString($post['searchString']);
    }
    /** Get ideas that user liked */
    else if ($post['request'] === 'getIdeasThatUserLiked') {
        $logger->debug("Requested service - GetIdeasThatUserLiked");
        $arendiService->getIdeasThatUserLiked($post['userId'], $post['startingIdeaIndex'], $post['numberOfIdeasToFetch']);
    }
    /** Get ideas that user liked */
    else if ($post['request'] === 'getMostLikedIdeas') {
        $logger->debug("Requested service - GetMostLikedIdeas");
        $arendiService->getMostLikedIdeas($post['startingIdeaIndex'], $post['numberOfIdeasToFetch']);
    }
    /** Get comments for idea */
    else if ($post['request'] === 'getCommentsForIdea') {
        $logger->debug("Requested service - GetCommentsForIdea");
        $arendiService->getCommentsForIdea($post['ideaId']);
    }
    /** Save idea */
    else if ($post['request'] === 'saveIdea') {
        $logger->debug("Requested service - SaveIdea");
        $arendiService->saveIdea($post);
    }
    /** Update idea */
    else if ($post['request'] === 'updateIdea') {
        $logger->debug("Requested service - UpdateIdea");
        $arendiService->updateIdea($post);
    }
    /** Set like for item */
    else if ($post['request'] === 'setLikeForItem') {
        $logger->debug("Requested service - SetLikeForItem");
        $arendiService->setLikeForItem($post);
    }
    /** Set comment for idea */
    else if ($post['request'] === 'setCommentForIdea') {
        $logger->debug("Requested service - SetCommentForIdea");
        $arendiService->setCommentForIdea($post);
    }
    /** Increment like for idea */
    else if ($post['request'] === 'incrementLikeForIdea') {
        $logger->debug("Requested service - IncrementLikeForIdea");
        $arendiService->incrementLikeForIdea($post);
    }
    /** Get likes count for item */
    else if ($post['request'] === 'getLikesCountForItem') {
        $logger->debug("Requested service - GetLikesCountForItem");
        $arendiService->getLikesCountForItem($post);
    }
    /** Get ideas count for user */
    else if ($post['request'] === 'getIdeasCountForUser') {
        $logger->debug("Requested service - GetIdeasCountForUser");
        $arendiService->getIdeasCountForUser($post['userId']);
    }
    /** Get comments count for idea */
    else if ($post['request'] === 'getCommentsCountForIdea') {
        $logger->debug("Requested service - GetCommentsCountForIdea");
        $arendiService->getCommentsCountForIdea($post['ideaId']);
    }
    /** Is item liken by user */
    else if ($post['request'] === 'isItemLikedByUser') {
        $logger->debug("Requested service - IsItemLikedByUser");
        $arendiService->isItemLikedByUser($post);
    }
    /** Remove like for item */
    else if ($post['request'] === 'removeLikeForItem') {
        $logger->debug("Requested service - RemoveLikeForItem");
        $arendiService->removeLikeForItem($post);
    } 
    /** Undefined service */
    else {
        $logger->debug("Requested service - Undefined\n");
        $arendiService->jsonResponseWithErrorMessage("Undefined service!");
    }
} 
/** No service field found */
else {
    $logger->error("Invalid service request!");
    $logger->error("Request must have a 'request' field!");
    $arendiService->jsonResponseWithErrorMessage(
        "Invalid service request! Request must have a 'request' field!"
    );
}
